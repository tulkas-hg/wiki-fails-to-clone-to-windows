# Wiki fails to clone to Windows


Cloning the [Wiki](https://gitlab.com/tulkas-hg/wiki-fails-to-clone-to-windows/-/wikis/home) to Windows fails due to in
incompatible file name of page [Titles with : or \\ lead to filenames that are invalid on Windows](https://gitlab.com/tulkas-hg/wiki-fails-to-clone-to-windows/-/wikis/Titles-with-:-or-%5C-lead-to-filenames-that-are-invalid-on-Windows).
